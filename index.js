const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const { User } = require('./app/models');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/users', async (req, res) => {
  const users = await User.findAll({
    attributes: ['id', 'name', 'email'],
  });
  res.status(200).json(users);
});

app.post('/users', async (req, res) => {
  const user = await User.create(req.body);
  res.status(201).json(user);
});

app.get('/users/:id', async (req, res) => {
  const user = await User.findOne({
    id: req.params.id,
    attributes: ['id', 'name', 'email'],
  });
  console.log(user);
  res.status(200).json(user);
});

app.put('/users/:id', async (req, res) => {
  const { name } = req.body;
  const { id } = req.params;
  const rowUpdated = await User.update({ name }, { where: { id } });

  if (rowUpdated >= 1) {
    res.status(200).json({ message: 'Updated with success!' });
  } else {
    res.status(500).json({ message: 'Update is failed!' });
  }
});

app.delete('/users/:id', async (req, res) => {
  await User.destroy({ where: { id: req.params.id } });
  res.status(204).json();
});

app.listen(3000);
